### Summary
* Flashcards web app using React.js, tracking scores utilizing React local state management.

<img src="https://media.giphy.com/media/zZhNgKpPTxECDAsUXJ/giphy.gif" width="600">

### Technologies
* ReactJS
* Bootstrap
### Using the App
* Hover over flashcard to view answer
* Click thumbs up or down to save correct and incorrect answers, half if almost got it
* Click next or previous to iterate through questions
* Start count over by clicking refresh icon

### TODO
* Look at all the flashcards at once in a list
* Shuffle flashcards ordering
* Bookmarking flashcards
* Questions with tags for filtering
* Filtering by type 
* Scoreboard
* Uploading q's with spreadsheet file or reference to googlesheet