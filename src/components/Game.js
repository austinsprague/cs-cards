import React, { Component } from 'react';
import CsCard from "./CsCard";
import Score from "./Score";
import {data} from "../db.json";
import { Row,Col,Button } from "react-bootstrap";


class Game extends Component {
    constructor(props) {
        super(props)

        this.state = {
            cardIndex: 0,
            correct: 0,
            incorrect: 0,
            half: 0
        }

        this.handleCardCount = this.handleCardCount.bind(this)
        this.handleScore = this.handleScore.bind(this)
        this.handleReset = this.handleReset.bind(this)
    }

    handleCardCount(count) {
        this.setState(prevState => {
            return { cardIndex: prevState.cardIndex + count  }
        })
    }

    handleScore(scoreState) {
        this.setState(prevState => {
            return { [scoreState]: prevState[scoreState] + 1 }
        })
    }

    handleReset() {
        this.setState({ correct: 0, incorrect: 0, half: 0, cardIndex: 0 })
    }

    render() {
        const { cardIndex, correct, incorrect, half } = this.state

        const previousIcon = <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-chevron-compact-left" viewBox="0 0 16 16">
            <path fill-rule="evenodd" d="M9.224 1.553a.5.5 0 0 1 .223.67L6.56 8l2.888 5.776a.5.5 0 1 1-.894.448l-3-6a.5.5 0 0 1 0-.448l3-6a.5.5 0 0 1 .67-.223z" />
        </svg>

        const nextIcon = <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-chevron-compact-right" viewBox="0 0 16 16">
            <path fill-rule="evenodd" d="M6.776 1.553a.5.5 0 0 1 .671.223l3 6a.5.5 0 0 1 0 .448l-3 6a.5.5 0 1 1-.894-.448L9.44 8 6.553 2.224a.5.5 0 0 1 .223-.671z" />
        </svg>

        const refreshIcon = <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-arrow-counterclockwise" viewBox="0 0 16 16">
            <path fill-rule="evenodd" d="M8 3a5 5 0 1 1-4.546 2.914.5.5 0 0 0-.908-.417A6 6 0 1 0 8 2v1z" />
            <path d="M8 4.466V.534a.25.25 0 0 0-.41-.192L5.23 2.308a.25.25 0 0 0 0 .384l2.36 1.966A.25.25 0 0 0 8 4.466z" />
        </svg>

        return (
            <div>
                <Row className="d-flex justify-content-center mt-5">
                    <Col xs={8} md={4}>
                        <Row className="d-flex ml-1">
                            <Col className="d-flex justify-content-start">
                                <p>Card {cardIndex + 1}</p>
                            </Col>
                            <Col className="d-flex justify-content-end">
                                <Button variant="flat" onClick={this.handleReset}>{refreshIcon}</Button>
                            </Col>
                        </Row>
                        <CsCard question={data[cardIndex].question} answer={data[cardIndex].answer}/>
                    </Col>
                    
                </Row>
                <Row className="mt-5">
                    <Col className="d-flex justify-content-center">
                        <Score correct={correct} incorrect={incorrect} half={half} scoreHandler={this.handleScore} resetHandler={this.handleReset} />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Button onClick={() => this.handleCardCount(-1)} disabled={cardIndex === 0} className="btn btn-light ">{previousIcon} Previous </Button>
                    </Col>
                    <Col>
                        <Button onClick={() => this.handleCardCount(1)} disabled={cardIndex === data.length - 1} className="btn btn-light ">Next {nextIcon}</Button>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default Game