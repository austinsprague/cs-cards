import React, { Component } from 'react';
import { Row, Col, Card, Button } from "react-bootstrap";

class CsCard extends Component {
    render() {
        const { question, answer } = this.props
        
        return (
            <Row className="d-flex">
                <Col className="flip-card ">
                    <div class="flip-card-inner d-flex align-items-center">
                        <div class="flip-card-front d-flex justify-content-center align-items-center">
                            <h5 style={{textTransform:'uppercase'}}>{question}</h5>
                        </div>
                        <div class="flip-card-back d-flex justify-content-center align-items-center">
                            <p style={{ textTransform: 'lowercase' }}>{answer}</p>
                        </div>
                    </div>
                </Col>
            </Row>
        )
    }

}

export default CsCard