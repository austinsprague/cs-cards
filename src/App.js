import Game from './components/Game'
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App">
      <Game/>
    </div>
  );
}

export default App;
